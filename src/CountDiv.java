/**
 * Write a function:
 * <p>
 * class Solution { public int solution(int A, int B, int K); }
 * <p>
 * that, given three integers A, B and K, returns the number of integers within the range [A..B] that are divisible by K, i.e.:
 * <p>
 * { i : A ≤ i ≤ B, i mod K = 0 }
 * <p>
 * For example, for A = 6, B = 11 and K = 2, your function should return 3, because there are three numbers divisible by 2 within the range [6..11], namely 6, 8 and 10.
 * <p>
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * A and B are integers within the range [0..2,000,000,000];
 * K is an integer within the range [1..2,000,000,000];
 * A ≤ B.
 */
public class CountDiv
{
    public static int solution(int A, int B, int K)
    {
        int add = 0;
        int subtract = 0;
        int start = 0;
        int stop = 0;

        int result = 0;

        if (A % K != 0)
        {
            if (A <= K) { add = K - A ;}
            else { add = K - (A % K); }
        }
        start = A + add;
        if (B % K != 0) { subtract = B % K; }
        stop = B - subtract;

        if (start <= B && stop >= A) { result = (stop - start) / K + 1; }

        return result;
    }

    public static void main(String[] args)
    {
        int[] data = {11, 345, 17};
//        int[] data = {6,11,2};
        int A = data[0];
        int B = data[1];
        int K = data[2];

        System.out.println(solution(A,B,K));
    }
}
