/**
 * An array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
 *
 * Your goal is to find that missing element.
 *
 * Write a function:
 *
 * class Solution { public int solution(int[] A); }
 *
 * that, given an array A, returns the value of the missing element.
 *
 * For example, given array A such that:
 *
 *   A[0] = 2
 *   A[1] = 3
 *   A[2] = 1
 *   A[3] = 5
 * the function should return 4, as it is the missing element.
 *
 * Write an efficient algorithm for the following assumptions:
 *
 * N is an integer within the range [0..100,000];
 * the elements of A are all distinct;
 * each element of array A is an integer within the range [1..(N + 1)].
 */

import java.util.Arrays;

public class L3_PermMissingElem_1
{
    public static void main(String[] args)
    {
        int[] A = {2, 3, 1, 5};

        int result = solution(A);

        System.out.println(result);
    }

    public static int solution(int[] A)
    {
        int result = 1;

        Arrays.sort(A);

        switch (A.length)
        {
            case 0:
                break;
            case 1:
                if (A[0] == 1)
                {
                    result = 2;
                }
                break;
            default:
            {
                if (A[A.length - 1] != (A.length + 1)) {result = (A.length + 1); }
                else
                {
                    for (int i = 0; i < A.length - 1; i++)
                    {
                        if (A[i + 1] - A[i] > 1)
                        {
                            return result = (A[i] + 1);
                        }
                    }
                }
            }
        }
        return result;
    }
}
