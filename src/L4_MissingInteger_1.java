/**
 * This is a demo task.
 *
 * Write a function:
 *
 * class Solution { public int solution(int[] A); }
 *
 * that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.
 *
 * For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.
 *
 * Given A = [1, 2, 3], the function should return 4.
 *
 * Given A = [−1, −3], the function should return 1.
 *
 * Write an efficient algorithm for the following assumptions:
 *
 * N is an integer within the range [1..100,000];
 * each element of array A is an integer within the range [−1,000,000..1,000,000].
 */

import java.util.Arrays;

public class L4_MissingInteger_1
{
    public static void main(String[] args)
    {
        int[] A = {-3,2};
        System.out.println(solution(A));
    }

    public static int solution(int[] A)
    {
        int result = 0;
        int temp1 = 0;
        int temp2 = 0;
        int index = 0;
        int finish = A.length - 1;

        Arrays.sort(A);

        switch (A.length)
        {
            case 1:
                if (A[0] <= 0) { result = 1; }
                if (A[0] == 1) { result = 2; }
                if (A[0] > 1) { result = 1; }
                break;
            case 2:
                if (A[0] <= 0)
                {
                    if (A[1] <= 0) { result = 1; }
                    if (A[1] == 1) { result = 2; }
                    if (A[1] > 1) { result = 1; }
                }
                if (A[0] >= 0)
                {
                    if (A[1] <= A[0] + 1) { result = A[1] + 1; }
                    if (A[1] > A[0] + 1) { result = A[0] + 1; }
                }
                break;
            default:
            {
                for (int i = 0; i <= finish; i++)
                {
                    if (A[i] > 0)
                    {
                        temp1 = A[i];
                        temp2 = A[i + 1];
                        break;
                    }
                    index++;
                }

                if (temp1 > 1)
                {
                    result = 1;
                }
                else
                {
                    while ((index + 1) < finish && (temp2 - temp1 <= 1))
                    {
                        index++;
                        temp1 = temp2;
                        temp2 = A[index + 1];
                    }
                    if (!((index + 1) < finish))
                    {
                        result = temp2 + 1;
                    }
                    if (!(temp2 - temp1 <= 1))
                    {
                        result = temp1 + 1;
                    }
                }
            }
            break;
        }
        return result;
    }
}
