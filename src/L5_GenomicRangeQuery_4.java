import java.util.*;

// Still too slow : |

public class L5_GenomicRangeQuery_4
{
    public static void main(String[] args)
    {
        //         "0 1 2 3 4  5  6";
        //         "2 1 3 2 2  4  1";
        //         "2 3 6 8 10 14 15";
        //         "0 2 3 6 8  10 14";

        String S = "CAGCCTA";
        int[] P = {2, 5, 0};
        int[] Q = {4, 5, 6};

        for (int i : solution(S, P, Q))
        {
            System.out.println(i + ", ");
        }
    }

    public static int[] solution(String S, int[] P, int[] Q)
    {
//        Map<Integer, Integer> map =
//                new TreeMap<>(
//                        new Comparator<Integer>()
//                        {
//                            @Override
//                            public int compare(Integer o1, Integer o2)
//                            {
//                                return o2.compareTo(o1);
//                            }
//                        }
//                );

        Map<Integer, Integer> treeMap =
                new TreeMap<>((Integer i1, Integer i2) -> i2.compareTo(i1));


        Map<Integer, Integer> linkedHashMap =
                new LinkedHashMap<>();

        for (int i = 0; i < P.length; i++)
        {
            treeMap.put(P[i], Q[i]);
            linkedHashMap.put(P[i], Q[i]);
        }

        for (Map.Entry<Integer, Integer> entry : treeMap.entrySet())
        {
            System.out.println(entry.getKey() + " --> " + entry.getValue());
        }

        System.out.println();

        for (Map.Entry<Integer, Integer> entry : linkedHashMap.entrySet())
        {
            System.out.println(entry.getKey() + " --> " + entry.getValue());
        }



        return null;
    }
}
