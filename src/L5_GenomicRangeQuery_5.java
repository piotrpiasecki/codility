// FASTEST!!! KRUUUUL!!!

public class L5_GenomicRangeQuery_5
{
    public static void main(String[] args)
    {
        //         "0 1 2 3 4  5  6";
        //         "2 1 3 2 2  4  1";
        //         "2 3 6 8 10 14 15";
        //         "0 2 3 6 8  10 14";

        String S = "CAGCCTA";
        int[] P = {2, 5, 0};
        int[] Q = {4, 5, 6};

        for (int i : solution(S, P, Q))
        {
            System.out.print(i + ", ");
        }
    }

    public static int[] solution(String S, int[] P, int[] Q)
    {
        int[][] dna = new int[S.length()][5];

        int[] result = new int[P.length];

        int start = 0;
        int distance = 0;

        int temp0 = 0;
        int tempA = 200_000;
        int tempC = 200_000;
        int tempG = 200_000;
        int tempT = 200_000;

        for (int i = S.length() - 1; i >= 0; i--)
        {
            switch (S.charAt(i))
            {
                case 'A':
                {
                    temp0 = 1;
                    tempA = 0;
                    tempC += 1;
                    tempG += 1;
                    tempT += 1;
                    break;
                }
                case 'C':
                {
                    temp0 = 2;
                    tempA += 1;
                    tempC = 0;
                    tempG += 1;
                    tempT += 1;
                    break;
                }
                case 'G':
                {
                    temp0 = 3;
                    tempA += 1;
                    tempC += 1;
                    tempG = 0;
                    tempT += 1;
                    break;
                }
                default:
                {
                    temp0 = 4;
                    tempA += 1;
                    tempC += 1;
                    tempG += 1;
                    tempT = 0;
                    break;
                }
            }
            dna[i][0] = temp0;
            dna[i][1] = tempA;
            dna[i][2] = tempC;
            dna[i][3] = tempG;
            dna[i][4] = tempT;
        }

        for (int i = 0; i < P.length; i++)
        {
            start = P[i];
            distance = Q[i] - P[i];

            if (dna[start][1] <= distance)
            {
                result[i] = 1;
            }
            else if (dna[start][2] <= distance)
            {
                result[i] = 2;
            }
            else if (dna[start][3] <= distance)
            {
                result[i] = 3;
            }
            else
            {
                result[i] = 4;
            }
        }

        for (int i = 0; i < dna.length; i++)
        {
            for (int j = 0; j < dna[i].length; j++)
            {
                System.out.print(dna[i][j] + " ");
            }
            System.out.println();
        }

        return result;
    }
}
