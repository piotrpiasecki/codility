import java.util.LinkedList;

/**
 * We draw N discs on a plane. The discs are numbered from 0 to N − 1. An array A of N non-negative integers, specifying the radiuses of the discs, is given. The J-th disc is drawn with its center at (J, 0) and radius A[J].
 * <p>
 * We say that the J-th disc and K-th disc intersect if J ≠ K and the J-th and K-th discs have at least one common point (assuming that the discs contain their borders).
 * <p>
 * The figure below shows discs drawn for N = 6 and A as follows:
 * <p>
 * A[0] = 1
 * A[1] = 5
 * A[2] = 2
 * A[3] = 1
 * A[4] = 4
 * A[5] = 0
 * <p>
 * <p>
 * There are eleven (unordered) pairs of discs that intersect, namely:
 * <p>
 * discs 1 and 4 intersect, and both intersect with all the other discs;
 * disc 2 also intersects with discs 0 and 3.
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs. The function should return −1 if the number of intersecting pairs exceeds 10,000,000.
 * <p>
 * Given array A shown above, the function should return 11, as explained above.
 * <p>
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [0..2,147,483,647].
 */

// Still too slow...

public class L6_NumberOfDiscIntersections_2
{
    public static int solution(int[] A)
    {
        int intersections = 0;
        int layers = 0;
        long left;
        long right;
        long nextLeft;
        long nextRight;

        LinkedList<Long> leftEnds = new LinkedList<>();
        LinkedList<Long> rightEnds = new LinkedList<>();

        leftEnds.addLast((long) -A[0]);
        rightEnds.addLast((long) A[0]);

        for (int i = 1; i < A.length; i++)
        {
            left = (long) i - (long) A[i];
            leftEnds.add(left);
            right = (long) A[i] + (long) i;
            rightEnds.add(right);
        }

        leftEnds.sort((Long l1, Long l2) -> l1.compareTo(l2));
        rightEnds.sort((Long l1, Long l2) -> l1.compareTo(l2));

        leftEnds.removeFirst();

        while (!leftEnds.isEmpty() && !rightEnds.isEmpty())
        {
            nextLeft = leftEnds.getFirst();
            nextRight = rightEnds.getFirst();

            if (nextLeft <= nextRight)
            {
                layers++;
                intersections += layers;
                leftEnds.removeFirst();
            }
            else
            {
                layers--;
                rightEnds.removeFirst();
            }
        }
        return intersections;
    }

    public static void main(String[] args)
    {
        int[] A = {1, 5, 2, 1, 4, 0};
        System.out.println(solution(A));
    }
}
