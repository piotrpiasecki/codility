import java.util.Arrays;

/**
 * We draw N discs on a plane. The discs are numbered from 0 to N − 1. An array A of N non-negative integers, specifying the radiuses of the discs, is given. The J-th disc is drawn with its center at (J, 0) and radius A[J].
 * <p>
 * We say that the J-th disc and K-th disc intersect if J ≠ K and the J-th and K-th discs have at least one common point (assuming that the discs contain their borders).
 * <p>
 * The figure below shows discs drawn for N = 6 and A as follows:
 * <p>
 * A[0] = 1
 * A[1] = 5
 * A[2] = 2
 * A[3] = 1
 * A[4] = 4
 * A[5] = 0
 * <p>
 * <p>
 * There are eleven (unordered) pairs of discs that intersect, namely:
 * <p>
 * discs 1 and 4 intersect, and both intersect with all the other discs;
 * disc 2 also intersects with discs 0 and 3.
 * Write a function:
 * <p>
 * class Solution { public int solution(int[] A); }
 * <p>
 * that, given an array A describing N discs as explained above, returns the number of (unordered) pairs of intersecting discs. The function should return −1 if the number of intersecting pairs exceeds 10,000,000.
 * <p>
 * Given array A shown above, the function should return 11, as explained above.
 * <p>
 * Write an efficient algorithm for the following assumptions:
 * <p>
 * N is an integer within the range [0..100,000];
 * each element of array A is an integer within the range [0..2,147,483,647].
 */

// Still too slow...

public class L6_NumberOfDiscIntersections_3
{
    public static int solution(int[] A)
    {
        long intersections = 0;
        int layers = 0;
        long leftEnd = Integer.MAX_VALUE;
        long rightEnd = Integer.MIN_VALUE;
        long tempLeft;
        long tempRight;

        for (int i = 0; i < A.length; i++)
        {
            tempLeft = i - A[i];
            if (tempLeft < leftEnd)
            {
                leftEnd = tempLeft;
            }

            tempRight = i + A[i];
            if (tempRight > rightEnd)
            {
                rightEnd = tempRight;
            }
        }

        long[] graph = new long[(int) (rightEnd - leftEnd + 1)];
        long[] points = new long[(int) (rightEnd - leftEnd + 1)];

        for (int i = 0; i < A.length; i++)
        {
            graph[(int) (i - A[i] - leftEnd)]++;
            graph[(int) (i + A[i] - leftEnd)]--;
            if (A[i] == 0)
            {
                points[(int) (i + A[i] - leftEnd)]++;
            }
        }

        for (int i = 1; i < graph.length; i++)
        {
            if (graph[i] > 0)
            {
                for (int j = 0; j < graph[i]; j++)
                {
                    layers++;
                    intersections += layers;
                }
            }
            else if (graph[i] < 0)
            {
                layers += graph[i];
            }

            if (points[i] > 0)
            {
                for (int j = 0; j < points[i]; j++)
                {
                    layers++;
                    intersections += layers;
                }
            }
        }

        if (intersections > 10_000_000)
        {
            intersections = -1;
        }

        return (int) intersections;
    }

    public static void main(String[] args)
    {
        int[] A = {1, 5, 2, 1, 4, 0};
        System.out.println(solution(A));
    }
}
